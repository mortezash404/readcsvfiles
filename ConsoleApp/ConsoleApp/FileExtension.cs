﻿using System;
using System.IO;
using System.Text.Json;

namespace ConsoleApp
{
    public static class FileExtension
    {
        public static string SetPath()
        {
            var directory = Directory.GetParent(Environment.CurrentDirectory).ToString();

            var newPath = Path.GetFullPath(Path.Combine(directory, @"..\..\"));

            var text = File.ReadAllText(Path.Join(newPath, @"appsettings.json"));

            var fileSetting = JsonSerializer.Deserialize<FileSetting>(text);

            var path = fileSetting.Path;

            return path;
        }
    }
}
