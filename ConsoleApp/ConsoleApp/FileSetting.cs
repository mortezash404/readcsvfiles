﻿using System.Text.Json.Serialization;

namespace ConsoleApp
{
    public class FileSetting
    {
        [JsonPropertyName("path")]
        public string Path { get; set; }
    }
}
