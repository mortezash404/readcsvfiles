﻿namespace ConsoleApp
{
    public class Person
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
    }
}
